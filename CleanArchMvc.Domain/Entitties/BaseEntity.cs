﻿namespace CleanArchMvc.Domain.Entitties
{
    public abstract class BaseEntity
    {
        public int Id { get; protected set; }
    }
}
